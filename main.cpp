#include <QApplication>
#include <QWidget>
#include <QMouseEvent>
#include <QIcon>
#include <QPixmap>
#include <QMimeData>
#include <QDrag>

class Widget : public QWidget {
public:
  Widget() : QWidget() {}
  void mouseMoveEvent(QMouseEvent* event_) {
    if(event_->buttons() & Qt::LeftButton) {
      QPixmap pix = QIcon::fromTheme(QStringLiteral("tools-wizard")).pixmap(128, 128);
      auto mimeData = new QMimeData;
      mimeData->setImageData(pix);
      auto drag = new QDrag(this);
      drag->setMimeData(mimeData);
      drag->setPixmap(pix);
      drag->exec(Qt::CopyAction);
    }
  }
};

int main(int argc, char *argv[]) {
  QApplication app(argc, argv);
  Widget w;
  w.show();
  return app.exec();
}
